import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static final int ONE_SECOND = 1;
    private static final int FIVE_SECONDS = 5;
    private static final int SEVEN_SECONDS = 7;
    private static final int SET_TIMER = 20;

    public static void main(String[] args) {
        // Задача №1,№2,№3 для проверки отсчитанного времени требуется указать SET_TIMER равный таймеру отсчёта
        // Задача №4 инициируется после выполнения трёх предыдущих задач

        // Задача №1,№2,№3
        Chrono chrono = new Chrono();

        List<Messenger> messengers = new ArrayList<>();
        messengers.add(new Messenger(ONE_SECOND, chrono));
        messengers.add(new Messenger(FIVE_SECONDS, chrono));
        messengers.add(new Messenger(SEVEN_SECONDS, chrono));

        new Thread(messengers.get(0), "Первый поток").start();
        new Thread(messengers.get(1), "Второй поток").start();
        new Thread(messengers.get(2), "Третий поток").start();
        chrono.countTime(messengers, SET_TIMER);

        // Задача №4
        BlockingQueue<String> queue = new LinkedBlockingQueue<>(2);
        String filePath = new File("resources", "WarAndPiece.txt").getAbsolutePath();
        Consumer consumer = new Consumer(queue);
        Thread consumerThread = new Thread(consumer);

        Thread producerThread = new Thread(() -> {
            Scanner scanner = null;
            String foundWord;
            try {
                scanner = new Scanner(new FileReader((filePath)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            while ((scanner.hasNextLine())) {
                String line = scanner.nextLine();
                Pattern pattern = Pattern.compile("([А-Яа-яЁё][А-Яа-яЁё]страдани*.)|(страдани*.)", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(line);

                while (matcher.find()) {
                    foundWord = matcher.group();

                    System.out.println("Producer передал слово: " + foundWord);
                    try {
                        queue.put(foundWord);
                    } catch (InterruptedException e) {
                        System.out.println("Ошибка в прерываниях: ".concat(e.getMessage()));
                    }
                }
            }
            consumerThread.interrupt();
        });

        producerThread.start();
        consumerThread.start();
        try {
            producerThread.join();
            consumerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
