import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    BlockingQueue<String> queue;

    public Consumer(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String value = queue.take();
                System.out.println("Consume получил слово: " + value);
            }
        } catch (InterruptedException e) {
            System.out.println("Останов работы приложения");
        }
    }
}
